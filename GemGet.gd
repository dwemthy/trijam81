extends Area2D

func _ready():
	connect("body_entered", self, "body_entered")
	
func body_entered(body):
	body.has_gem = true
	get_tree().call_group("mine", "gem_get")
