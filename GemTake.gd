extends Area2D

func _ready():
	connect("body_entered", self, "body_entered")
	
func body_entered(body):
	if body.has_gem:
		get_tree().call_group("mine", "gem_take")
		if body.has_method("ascend"):
			body.ascend()
