extends Node2D

export(PackedScene) var Miner

var gems = 5
var dead_miners = 0

func _ready():
	add_to_group("mine")

func gem_take():
	$Gems.get_child(gems - 1).modulate = Color(1.0, 1.0, 1.0, 0.0)
	gems -= 1
	if gems <= 0:
		$Camera2D/CanvasLayer/GameOver.show()
		get_tree().paused = true

func gem_get():
	$Gems.get_child(gems - 1).modulate = Color(1.0, 1.0, 1.0, 0.5)
	
func gem_recover():
	$Gems.get_child(gems - 1).modulate = Color(1.0, 1.0, 1.0, 1.0)

func spawn_miner():
	if Miner:
		var miner = Miner.instance()
		miner.position = $GemTake.position
		miner.set_move_interval(dead_miners)
		add_child(miner)

func miner_died():
	dead_miners += 1
	$Camera2D/CanvasLayer/Score.text = String(dead_miners)
	spawn_miner()
