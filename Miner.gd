extends KinematicBody2D

export var move_interval = 0.5

var has_gem = false
var dying = false

var since_move = 0

func _ready():
	$AnimationPlayer.connect("animation_finished", self, "animation_finished")
	
func set_move_interval(factor):
	move_interval = move_interval / factor

func _physics_process(delta):
	if not dying:
		since_move += delta
		if since_move >= move_interval:
			since_move = 0
			var move_amount = 8
			if has_gem:
				move_amount = -8
				if not $Sprite/Gem.visible:
					$Sprite/Gem.show()
				$Sprite.flip_h = true
			move_and_collide(Vector2(move_amount, 0), false)
		
func die():
	dying = true
	$AnimationPlayer.current_animation = "Die"

func ascend():
	dying = true
	$AnimationPlayer.current_animation = "Ascend"
		
func animation_finished(which):
	if which == "Die":
		if has_gem:
			get_tree().call_group("mine", "gem_recover")
		get_tree().call_group("mine", "miner_died")
		queue_free()
	elif which == "Ascend":
		get_tree().call_group("mine", "spawn_miner")
		queue_free()
