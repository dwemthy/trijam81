extends Area2D

# Called when the node enters the scene tree for the first time.
func _ready():
	$AnimationPlayer.connect("animation_finished", self, "animation_finished")
	connect("body_entered", self, "body_entered")

func body_entered(body):
	if $AnimationPlayer.current_animation == "Attack":
		if body.has_method("die"):
			body.die()
		else:
			body.queue_free()

func _unhandled_input(event):
	if event.is_action_pressed("ui_accept") and not $AnimationPlayer.is_playing():
		$AnimationPlayer.current_animation = "Attack"
		for body in get_overlapping_bodies():
			if body.has_method("die"):
				body.die()
			else:
				body.queue_free()

func animation_finished(which):
	if which == "Attack":
		$AnimationPlayer.current_animation = "Recoil"
